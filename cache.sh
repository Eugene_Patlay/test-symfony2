#!/bin/sh

app/console cache:clear --no-debug
rm -rf app/cache/**
#app/console cache:flush default
chmod -R 777 app/cache
chmod -R 777 app/logs
