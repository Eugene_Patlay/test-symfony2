$(document).on('click', ".filter a, .sort a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
        type: 'GET',
        url: url,
        success: function(data){
            $('#main').html(data);
        }
    })
});
