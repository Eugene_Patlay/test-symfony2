<?php
namespace Parfums\ProductsBundle\Util;

use Parfums\ProductsBundle\ElasticRepository\ElasticProductRepository;

class SearchProductUtil
{
    /**
     * @var \Parfums\ProductsBundle\ElasticRepository\ElasticProductRepository
     */
    private $elasticProductRepository;


    public function __construct(ElasticProductRepository $elasticProductRepository)
    {
        $this->elasticProductRepository = $elasticProductRepository;
    }

    /**
     * Возвращает отфильтрованный массив товаров
     * @param string $params строка с параметрами фильтрации
     * @param string $query строка с параметром поиска
     * @return array массив товаров
     */
    public function getProducts($params, $query)
    {
        return $this->elasticProductRepository->getProducts($params, $query);
    }
}