<?php
namespace Parfums\ProductsBundle\Util;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Parfums\ProductsBundle\Service\Helper;
use Parfums\ProductsBundle\ElasticRepository\ElasticProductFilterRepository;

class SearchProductFilterUtil
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    private $router;

    /**
     * @var \Parfums\ProductsBundle\ElasticRepository\ElasticProductFilterRepository
     */
    private $elasticProductFilterRepository;

    /**
     * @var array массив фасетов для значений фильтра
     */
    private $facetValueResult = array();

    /**
     * @var \Elastica\ResultSet
     */
    private $resultSet;

    public function __construct(EntityManager $em, UrlGeneratorInterface $router, ElasticProductFilterRepository $elasticProductFilterRepository)
    {
        $this->em = $em;
        $this->router = $router;
        $this->elasticProductFilterRepository = $elasticProductFilterRepository;
    }

    /**
     * Создание фасета по определенному параметру для фильтра, исходя из уже отфильтраванных значений
     * @param array $filter массив фильтра
     * @param string $from по какому параметру делаем фасет
     * @param string $query строка с параметром поиска
     * @return array массив фасетов по параметру
     */
    public function getFacetValueResult($filter, $from, $query)
    {
        $elasticFacet = array();
        $this->resultSet = $this->elasticProductFilterRepository->getFacetValueResult($filter, $from, $query);
        $elasticFacets = $this->resultSet->getFacets();
        foreach ($elasticFacets as $elasticFacetKey => $elasticFacetValue) {
            foreach ($elasticFacetValue['terms'] as $value) {
                $elasticFacet[$value['term']] = $value['count'];
            }
        }
        return $elasticFacet;
    }

    /**
     * Создание массива со ссылками для фильтрации
     * @param array $arrayParams текущие параметры фильтра
     * @param string $query текущие параметры строки поиска
     * @return array $filterRequest массив сылок для фильтрации
     */
    public function getOption($arrayParams, $query)
    {
        $arrayParams = Helper::formatParamsArray($arrayParams);
        unset($arrayParams['sort']);//убираем из массива параметров фильтрации параметры сортировки
        $filterRequest = array();
        $this->facetValueResult['category'] = $this->getFacetValueResult($arrayParams, 'category', $query);
        $filterRequest['category'] = $this->getFilterCategoryConstruct($arrayParams);
        $this->facetValueResult['brand'] = $this->getFacetValueResult($arrayParams, 'brand', $query);
        $filterRequest['brand'] = $this->getFilterBrandConstruct($arrayParams);
        $this->facetValueResult['properties'] = $this->getFacetValueResult($arrayParams, 'properties', $query);
        $filterRequest['params'] = $this->getFilterPropertyValueConstruct($arrayParams);
        return $filterRequest;
    }

    /**
     * Создание массива ссылок для фильтрации по категориям
     * @param array $arrayParams текущие параметры фильтра
     * @return array $filters массив сылок для фильтрации по категориям
     */
    protected function getFilterCategoryConstruct($arrayParams)
    {
        $inParams = array();
        if (isset($arrayParams['category'])) {
            $inParams = explode(',', $arrayParams['category']);
        }
        $modelFilter = $this->em->getRepository('ParfumsProductsBundle:Products')->getFilterCategory();
        $filters = array();
        foreach ($modelFilter as $filter) {
            $filterArray['id'] = $filter['categoryId'];
            $filterArray['name'] = $filter['categoryName'];
            if (in_array($filterArray['id'], $inParams)) {
                $filterArray['active'] = true;
            } else {
                $filterArray['active'] = false;
            }
            $filterArray['link'] = $this->getFilterLink($filterArray, $inParams, $arrayParams, 'category');
            $filters[] = $filterArray;
        }

        return $filters;
    }

    /**
     * Создание массива ссылок для фильтрации по брендам
     * @param array $arrayParams текущие параметры фильтра
     * @return array $filters массив сылок для фильтрации по брендам
     */
    protected function getFilterBrandConstruct($arrayParams)
    {
        $filters = array();
        $inParams = array();
        if (isset($arrayParams['brand'])) {
            $inParams = explode(',', $arrayParams['brand']);
        }
        $modelFilter = $this->em->getRepository('ParfumsProductsBundle:Products')->getFilterBrand();

        foreach ($modelFilter as $filter) {
            $filterArray['id'] = $filter['brandId'];
            $filterArray['name'] = $filter['brandName'];
            if (in_array($filterArray['id'], $inParams)) {
                $filterArray['active'] = true;
            } else {
                $filterArray['active'] = false;
            }
            $filterArray['link'] = $this->getFilterLink($filterArray, $inParams, $arrayParams, 'brand');
            $filters[] = $filterArray;
        }

        return $filters;
    }

    /**
     * Создание массива ссылок для фильтрации по параметрам
     * @param array $arrayParams текущие параметры фильтра
     * @return array $filters массив сылок для фильтрации по параметрам
     */
    protected function getFilterPropertyValueConstruct($arrayParams)
    {
        $inParams = array();
        if (isset($arrayParams["properties"])) {
            $inParams = explode(',', $arrayParams["properties"]);
        }
        $modelFilter = $this->em->getRepository('ParfumsProductsBundle:Properties')->getFilterProperty($arrayParams);

        $filters = array();
        $propValues = array();
        $filtersName = array();
        foreach ($modelFilter as $filter) {
            $filtersName[$filter['id']] = array(
                'id' => $filter['id'],
                'name' => $filter['name']);
            $propValues[$filter['id']][] = array(
                'id' => $filter['propValId'],
                'value'=>$filter['value'],
                'link' => $this->getFilterLink(array(
                    'id' => $filter['propValId'],
                    'name' => $filter['value'],
                    'active' => ((in_array($filter['propValId'], $inParams)) ? true : false)
                ), $inParams, $arrayParams, 'properties'),
            );
        }

        foreach ($filtersName as $filter) {
            $filters[$filter['id']] = array('id' => $filter['id'], 'name' => $filter['name'], 'values' => $propValues[$filter['id']]);
        }

        return $filters;
    }

    /**
     * Генерирование ссылок для фильтрации
     * @param array $filterArray массив с id, наименованием и статусом параметра для фильтрации
     * @param array $inParams массив с уже присутствующими в фильте id'шками данного параметра
     * @param array $arrayParams массив со всем параметрами фильтра
     * @param string $from наименование параметра, для которого генерируем ссылку
     * @return string ссылка для фильтрации
     */
    protected function getFilterLink($filterArray, $inParams, $arrayParams, $from)
    {
        $generateParams = '';
        if (array_key_exists($from, $arrayParams)) {
            unset($arrayParams["$from"]);
            foreach ($arrayParams as $key => $value) {
                $generateParams .= '/' . $key . '/' . $value;
            }
            if ($filterArray['active']) {
                $inParamsDeleted = array();
                foreach ($inParams as $key => $idParam) {
                    if ($idParam != $filterArray['id'])
                        $inParamsDeleted[] = $idParam;
                }
                if (!empty($inParamsDeleted))
                    $generateParams .= "/$from/" . implode(',', $inParamsDeleted);
            } else {
                $inParamsAdd = $inParams;
                $inParamsAdd[] = $filterArray['id'];
                $generateParams .= "/$from/" . implode(',', $inParamsAdd);
            }
        } else {
            foreach ($arrayParams as $key => $value) {
                $generateParams .= '/' . $key . '/' . $value;
            }
            $generateParams .= "/$from/" . $filterArray['id'];
        }

        return array(
            'generateParams' => substr($generateParams, 1),
            'facet' => '(' . ((isset($this->facetValueResult["$from"][$filterArray['id']])) ? $this->facetValueResult["$from"][$filterArray['id']] : 0) . ')',
            'filterActive' => $filterArray['active'],
        );
    }

}