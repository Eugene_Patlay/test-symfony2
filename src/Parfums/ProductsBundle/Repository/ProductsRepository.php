<?php

namespace Parfums\ProductsBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ProductsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductsRepository extends EntityRepository
{
    public function getFilterCategory()
    {
        return $this->createQueryBuilder('prod')
            ->select('cat.id AS categoryId, cat.name AS categoryName')
            ->leftJoin('prod.category', 'cat')
            ->groupBy('categoryId')
            ->orderBy('cat.name', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getFilterBrand()
    {
        return $this->createQueryBuilder('prod')
            ->select('brand.id AS brandId, brand.name AS brandName')
            ->leftJoin('prod.brand', 'brand')
            ->groupBy('brand.name')
            ->orderBy('brand.name', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
}
