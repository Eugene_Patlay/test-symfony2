<?php

namespace Parfums\ProductsBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PropertiesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PropertiesRepository extends EntityRepository
{
    public function getFilterProperty($arrayParams = null)
    {
        $query = $this->createQueryBuilder('prop')
            ->select('prop.id, prop.name, propVal.id AS propValId, propVal.value as value')
            ->innerJoin('prop.propertyValue', 'propVal')
            ->innerJoin('propVal.productsMany', 'prod')
            ->leftJoin('prod.category', 'cat')
            ->leftJoin('prod.brand', 'brand')
            ->groupBy('value');
        if (!empty($arrayParams['category'])) {
            $query->where('cat.id IN (' . $arrayParams['category'] . ')');
        }
        return $query->getQuery()->getArrayResult();
    }
}
