<?php

namespace Parfums\ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 */
class Products
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Products
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Products
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Products
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $propertyValue;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->propertyValue = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add propertyValue
     *
     * @param \Parfums\ProductsBundle\Entity\PropertyValue $propertyValue
     * @return Products
     */
    public function addPropertyValue(\Parfums\ProductsBundle\Entity\PropertyValue $propertyValue)
    {
        $this->propertyValue[] = $propertyValue;

        return $this;
    }

    /**
     * Remove propertyValue
     *
     * @param \Parfums\ProductsBundle\Entity\PropertyValue $propertyValue
     */
    public function removePropertyValue(\Parfums\ProductsBundle\Entity\PropertyValue $propertyValue)
    {
        $this->propertyValue->removeElement($propertyValue);
    }

    /**
     * Get propertyValue
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }
    /**
     * @var \Parfums\ProductsBundle\Entity\Category
     */
    private $category;

    /**
     * @var \Parfums\ProductsBundle\Entity\Brand
     */
    private $brand;


    /**
     * Set category
     *
     * @param \Parfums\ProductsBundle\Entity\Category $category
     * @return Products
     */
    public function setCategory(\Parfums\ProductsBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Parfums\ProductsBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brand
     *
     * @param \Parfums\ProductsBundle\Entity\Brand $brand
     * @return Products
     */
    public function setBrand(\Parfums\ProductsBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Parfums\ProductsBundle\Entity\Brand 
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
