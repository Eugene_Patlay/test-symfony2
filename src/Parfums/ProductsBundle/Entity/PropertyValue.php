<?php

namespace Parfums\ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyValue
 */
class PropertyValue
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \Parfums\ProductsBundle\Entity\Properties
     */
    private $properties;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productsMany;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productsMany = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return PropertyValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set properties
     *
     * @param \Parfums\ProductsBundle\Entity\Properties $properties
     * @return PropertyValue
     */
    public function setProperties(\Parfums\ProductsBundle\Entity\Properties $properties = null)
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Get properties
     *
     * @return \Parfums\ProductsBundle\Entity\Properties 
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Add productsMany
     *
     * @param \Parfums\ProductsBundle\Entity\Products $productsMany
     * @return PropertyValue
     */
    public function addProductsMany(\Parfums\ProductsBundle\Entity\Products $productsMany)
    {
        $this->productsMany[] = $productsMany;

        return $this;
    }

    /**
     * Remove productsMany
     *
     * @param \Parfums\ProductsBundle\Entity\Products $productsMany
     */
    public function removeProductsMany(\Parfums\ProductsBundle\Entity\Products $productsMany)
    {
        $this->productsMany->removeElement($productsMany);
    }

    /**
     * Get productsMany
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductsMany()
    {
        return $this->productsMany;
    }
}
