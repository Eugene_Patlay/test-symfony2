<?php

namespace Parfums\ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Properties
 */
class Properties
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Properties
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Properties
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $propertyValue;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->propertyValue = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add propertyValue
     *
     * @param \Parfums\ProductsBundle\Entity\PropertyValue $propertyValue
     * @return Properties
     */
    public function addPropertyValue(\Parfums\ProductsBundle\Entity\PropertyValue $propertyValue)
    {
        $this->propertyValue[] = $propertyValue;

        return $this;
    }

    /**
     * Remove propertyValue
     *
     * @param \Parfums\ProductsBundle\Entity\PropertyValue $propertyValue
     */
    public function removePropertyValue(\Parfums\ProductsBundle\Entity\PropertyValue $propertyValue)
    {
        $this->propertyValue->removeElement($propertyValue);
    }

    /**
     * Get propertyValue
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }
}
