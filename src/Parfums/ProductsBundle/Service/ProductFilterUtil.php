<?php
namespace Parfums\ProductsBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductFilterUtil
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    private $router;

    private $facetValues;

    public function __construct(EntityManager $em, UrlGeneratorInterface $router)
    {
        $this->em = $em;
        $this->router = $router;
    }

    public function getProductsFilter($arrayParams)
    {
        $this->facetValues = $this->getFacetValues($arrayParams);
        $filterRequest['filterArray']['category'] = $this->getFilterCategoryConstruct($arrayParams);
        $filterRequest['filterArray']['brand'] = $this->getFilterBrandConstruct($arrayParams);
        $filterRequest['filterArray']['params'] = $this->getFilterPropertyValueConstruct($arrayParams);
        return $filterRequest;
    }

    protected function getFilterCategoryConstruct($arrayParams)
    {
        $inParams = array();
        if (isset($arrayParams['category'])) {
            $inParams = explode(',', $arrayParams['category']);
        }
        $modelFilter = $this->em->getRepository('ParfumsProductsBundle:Products')->getFilterCategory();

        $filters = array();
        foreach ($modelFilter as $filter) {
            $filterArray['id'] = $filter['categoryId'];
            $filterArray['name'] = $filter['categoryName'];
            if (in_array($filterArray['id'], $inParams)) {
                $filterArray['active'] = true;
            } else {
                $filterArray['active'] = false;
            }
            $filterArray['link'] = $this->getFilterLink($filterArray, $inParams, $arrayParams, 'category');
            $filters[] = $filterArray;
        }
        return $filters;
    }

    protected function getFilterBrandConstruct($arrayParams)
    {
        $filters = array();
        $inParams = array();
        if (isset($arrayParams['brand'])) {
            $inParams = explode(',', $arrayParams['brand']);
        }
        $modelFilter = $this->em->getRepository('ParfumsProductsBundle:Products')->getFilterBrand();

        foreach ($modelFilter as $filter) {
            $filterArray['id'] = $filter['brandId'];
            $filterArray['name'] = $filter['brandName'];
            if (in_array($filterArray['id'], $inParams)) {
                $filterArray['active'] = true;
            } else {
                $filterArray['active'] = false;
            }
            $filterArray['link'] = $this->getFilterLink($filterArray, $inParams, $arrayParams, 'brand');
            $filters[] = $filterArray;
        }

        return $filters;
    }

    protected function getFilterPropertyValueConstruct($arrayParams)
    {
        $inParams = array();
        if (isset($arrayParams["params"])) {
            $inParams = explode(',', $arrayParams["params"]);
        }
        $modelFilter = $this->em->getRepository('ParfumsProductsBundle:Properties')->getFilterProperty($arrayParams);

        $filters = array();
        $propValues = array();
        $filtersName = array();
        foreach ($modelFilter as $filter) {
            $filtersName[$filter['id']] = array(
                'id' => $filter['id'],
                'name' => $filter['name']);

            $propValues[$filter['id']][] = array(
                'id' => $filter['propValId'],
                'link' => $this->getFilterLink(array(
                    'id' => $filter['propValId'],
                    'name' => $filter['value'],
                    'active' => ((in_array($filter['propValId'], $inParams)) ? true : false)
                ), $inParams, $arrayParams, 'params'),
            );
        }

        foreach ($filtersName as $filter) {
            $filters[$filter['id']] = array('id' => $filter['id'], 'name' => $filter['name'], 'values' => $propValues[$filter['id']]);
        }
        return $filters;
    }

    protected function getFilterLink($filterArray, $inParams, $arrayParams, $from)
    {
        $generateParams = '';
        if (array_key_exists($from, $arrayParams)) {
            unset($arrayParams["$from"]);
            foreach ($arrayParams as $key => $value) {
                $generateParams .= '/' . $key . '/' . $value;
            }
            if ($filterArray['active']) {
                $inParamsDeleted = array();
                foreach ($inParams as $key => $idParam) {
                    if ($idParam != $filterArray['id'])
                        $inParamsDeleted[] = $idParam;
                }
                if (!empty($inParamsDeleted))
                    $generateParams .= "/$from/" . implode(',', $inParamsDeleted);
            } else {
                $inParamsAdd = $inParams;
                $inParamsAdd[] = $filterArray['id'];
                $generateParams .= "/$from/" . implode(',', $inParamsAdd);
            }
        } else {
            foreach ($arrayParams as $key => $value) {
                $generateParams .= '/' . $key . '/' . $value;
            }
            $generateParams .= "/$from/" . $filterArray['id'];
        }
        return $this->generateLink(substr($generateParams, 1), $filterArray, $from);

    }

    protected function generateLink($generateParams, $filterArray, $from)
    {
        return '<a id="filter-' . $from . '-' . $filterArray['id'] . '" href="' . $this->router->generate('parfums_products_products',
            array('params' => $generateParams)) . '">'
        . '<input type="checkbox" for="filter-' . $from . '-' . $filterArray['id'] . '" ' . (($filterArray['active']) ? "checked" : "") . '/> '
        . $filterArray['name']
        . ' (' . ((isset($this->facetValues["$from"][$filterArray['id']])) ? $this->facetValues["$from"][$filterArray['id']]['count'] : 0) . ')'
        . '</a>';
    }

    protected function getFacetValues($arrayParams)
    {
        $arrayFilter = array();
        $queryCatalog = $this->em->getRepository('ParfumsProductsBundle:Category')
            ->createQueryBuilder('category')
            ->innerJoin('category.products', 'prod')
            ->innerJoin('prod.brand', 'brand')
            ->innerJoin('prod.propertyValue', 'params');
        $queryCatalog->select('category.name as categoryName, category.id as categoryId');
        $queryCatalog->groupBy('prod.id');
        foreach ($arrayParams as $key => $value) {
            if ($key != 'category') {
                $queryCatalog->andWhere($key . '.id IN (' . $value . ')');
            }
        }
        $productsFilterCategory = $queryCatalog->getQuery()->getArrayResult();
        foreach ($productsFilterCategory as $oneFilter) {
            if (!isset($arrayFilter['category'][$oneFilter['categoryId']])) {
                $arrayFilter['category'][$oneFilter['categoryId']] = array(
                    'name' => $oneFilter['categoryName'],
                    'count' => 1
                );
            } else {
                $arrayFilter['category'][$oneFilter['categoryId']]['count']++;
            }
        }

        $queryBrand = $this->em->getRepository('ParfumsProductsBundle:Brand')
            ->createQueryBuilder('brand')
            ->innerJoin('brand.products', 'prod')
            ->innerJoin('prod.category', 'category')
            ->innerJoin('prod.propertyValue', 'params');
        $queryBrand->select('brand.name as brandName, brand.id as brandId');
        $queryBrand->groupBy('prod.id');
        foreach ($arrayParams as $key => $value) {
            if ($key != 'brand') {
                $queryBrand->andWhere($key . '.id IN (' . $value . ')');
            }
        }
        $productsFilterBrand = $queryBrand->getQuery()->getArrayResult();
        foreach ($productsFilterBrand as $oneFilter) {
            if (!isset($arrayFilter['brand'][$oneFilter['brandId']])) {
                $arrayFilter['brand'][$oneFilter['brandId']] = array(
                    'name' => $oneFilter['brandName'],
                    'count' => 1
                );
            } else {
                $arrayFilter['brand'][$oneFilter['brandId']]['count']++;
            }
        }

        $queryParams = $this->em->getRepository('ParfumsProductsBundle:Properties')
            ->createQueryBuilder('prop')
            ->leftJoin('prop.propertyValue', 'params')
            ->leftJoin('params.productsMany', 'prod')
            ->leftJoin('prod.category', 'category')
            ->leftJoin('prod.brand', 'brand');
        $queryParams->select('params.value as paramsName, params.id as paramsId');
        foreach ($arrayParams as $key => $value) {
            if ($key != 'params') {
                $queryParams->andWhere($key . '.id IN (' . $value . ')');
            }
        }
        $productsParams = $queryParams->getQuery()->getArrayResult();
        foreach ($productsParams as $oneFilter) {
            if (!isset($arrayFilter['params'][$oneFilter['paramsId']])) {
                $arrayFilter['params'][$oneFilter['paramsId']] = array(
                    'name' => $oneFilter['paramsName'],
                    'count' => 1
                );
            } else {
                $arrayFilter['params'][$oneFilter['paramsId']]['count']++;
            }
        }

        return $arrayFilter;
    }
}