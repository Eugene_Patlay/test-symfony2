<?php
namespace Parfums\ProductsBundle\Service;

class Helper
{
    /**
     * Форматируем параметры поиска из строки в массив по регулярному выражению
     * @param string $params строка с параметрами
     * @return array массив с параметрами
     */
    public static function formatParamsArray($params)
    {
        $arrayParams = array();
        foreach (array_chunk(explode('/', $params), 2) as $param) {
            if (isset($param[0]) && isset($param[1]))
                $arrayParams[preg_replace('/[^0-9A-Za-z]+/','',$param[0])] = preg_replace('/[^0-9A-Za-z,]+/','',$param[1]);
        }
        return $arrayParams;
    }

    /**
     * @return array Асоциативный массив, где ключ=параметр в строке запроса, а значение=соответсвующее значение в базе
     */
    public static function getArrayFilter(){
        return array(
            'category' => 'category.id',
            'brand' => 'brand.id',
            'properties' => 'propertyValue.id'
        );
    }

}