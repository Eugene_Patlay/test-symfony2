<?php
namespace Parfums\ProductsBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Parfums\ProductsBundle\Service\ProductFilterUtil;
use Parfums\ProductsBundle\Service\Helper;

class ProductUtil
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * @var \Parfums\ProductsBundle\Service\ProductFilterUtil
     */
    private $productFilterUtil;

    public function __construct(EntityManager $em, Session $session, ProductFilterUtil $productFilterUtil)
    {
        $this->em = $em;
        $this->session = $session;
        $this->productFilterUtil = $productFilterUtil;
    }

    public function getProductsAndFilter($params)
    {
        $arrayParams = Helper::formatParamsArray($params);
        $query = $this->em->getRepository('ParfumsProductsBundle:Products')
            ->createQueryBuilder('prod')
            ->select('prod.id, prod.name, prod.description, prod.price, category.name AS categoryName, brand.name AS brandName')
            ->leftJoin('prod.category', 'category')
            ->leftJoin('prod.brand', 'brand')
            ->innerJoin('prod.propertyValue', 'params');
        $query->groupBy('prod.id');

        if (!empty($arrayParams['sort'])) {
            if ($this->session->get('orderBy') != null) {
                if ($this->session->get('order') == $arrayParams['sort'] && $this->session->get('orderBy') == 'DESC') {
                    $this->session->set('orderBy', 'ASC');
                } elseif ($this->session->get('order') == $arrayParams['sort'] && $this->session->get('orderBy') == 'ASC') {
                    $this->session->set('orderBy', 'DESC');
                }
            } else {
                $this->session->set('orderBy', 'DESC');
            }
            $this->session->set('order', $arrayParams['sort']);
            unset($arrayParams['sort']);
        }

        foreach ($arrayParams as $key => $value) {
            $query->andWhere($key . '.id IN (' . $value . ')');
        }

        if ($this->session->get('order') != null) {
            $query->orderBy('prod.' . $this->session->get('order'), $this->session->get('orderBy'));
        }

        $products = $query->getQuery()->getArrayResult();
        $filter = $this->productFilterUtil->getProductsFilter($arrayParams);
        $filter['sortRequest'] = str_replace(array('sort/name/', 'sort/price/'), '', $params);
        $productsAndFilter = array('products' => $products, 'filter' => $filter);
        return $productsAndFilter;
    }
}