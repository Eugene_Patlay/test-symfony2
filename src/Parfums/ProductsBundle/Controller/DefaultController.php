<?php

namespace Parfums\ProductsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ParfumsProductsBundle:Default:index.html.twig');
    }

    public function productsAction($params, Request $request)
    {
        $productsAndFilter = $this->get('parfums.products.productUtil')->getProductsAndFilter($params);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse($this->renderView('ParfumsProductsBundle:Default:_products.html.twig', array(
                'productsAndFilter' => $productsAndFilter))
            );
        } else {
            return $this->render('ParfumsProductsBundle:Default:products.html.twig', array(
                    'productsAndFilter' => $productsAndFilter)
            );
        }
    }

    public function viewAction($id)
    {
        $product = $this->getDoctrine()->getRepository('ParfumsProductsBundle:Products')->find($id);
        if (!$product) {
            throw $this->createNotFoundException('No product found id: ' . $id);
        }
        return $this->render('ParfumsProductsBundle:Default:view.html.twig', array('product' => $product));
    }

    public function elasticProductsAction($params, Request $request)
    {
        $query = $request->get('q');
        $option = $this->get('parfums.products.searchProductFilterUtil')->getOption($params, $query);//получаем масив элементов для фильтра
        $products = $this->get('parfums.products.searchProductUtil')->getProducts($params, $query);//получаем массив элементов для списка товаров
        $sortRequest = str_replace(array('sort/name/', 'sort/price/'), '', $params);//возвращаем реквест без параметров сортировки
        $arrayView = array('option'=>$option, 'products' => $products, 'sortRequest'=>$sortRequest, 'query'=>$query);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse($this->renderView('ParfumsProductsBundle:Default:_elastic-products.html.twig', $arrayView));//в случае ajax запроса возвращаем json
        } else {
            return $this->render('ParfumsProductsBundle:Default:elastic-products.html.twig', $arrayView);
        }
    }
}
