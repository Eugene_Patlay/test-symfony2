<?php
namespace Parfums\ProductsBundle\ElasticRepository;

use Parfums\ProductsBundle\Service\Helper;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\ElasticaBundle\Finder\TransformedFinder;

class ElasticProductRepository
{
    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * @var \FOS\ElasticaBundle\Finder\TransformedFinder
     */
    private $fosElasticProducts;

    /**
     * @var array $arrayFilter асоциативный массив, где ключ=параметр в строке запроса, а значение=соответсвующее значение в базе
     */
    private $arrayFilter = array();

    public function __construct(TransformedFinder $fosElasticFinderExampleProducts, Session $session)
    {
        $this->fosElasticProducts = $fosElasticFinderExampleProducts;
        $this->session = $session;
        $this->arrayFilter = Helper::getArrayFilter();
    }

    /**
     * Возвращает отфильтрованный массив товаров
     * @param string $params строка с параметрами фильтрации
     * @param string $query строка с параметрами поиска
     * @return array массив товаров
     */
    public function getProducts($params, $query)
    {
        $filter = Helper::formatParamsArray($params); //Преобразовуем сроку фильтрации в массив
        //манипуляции с сортировкой, тут сохраняется в сесии как именно будет сортировать (по убыванию или возрастанию)
        if (!empty($filter['sort'])) {
            if ($this->session->get('orderBy') != null) {
                if ($this->session->get('order') == $filter['sort'] && $this->session->get('orderBy') == 'desc') {
                    $this->session->set('orderBy', 'asc');
                } elseif ($this->session->get('order') == $filter['sort'] && $this->session->get('orderBy') == 'asc') {
                    $this->session->set('orderBy', 'desc');
                }
            } else {
                $this->session->set('orderBy', 'desc');
            }
            $this->session->set('order', $filter['sort']);
            unset($filter['sort']);
        }
        //достаем товары
        if (!empty($filter)) {
            $orOuter = new \Elastica\Filter\Bool();
            foreach ($filter as $filterKey => $filterValue) {
                $optionKeyTerm = new \Elastica\Filter\Terms();
                $optionKeyTerm->setTerms($this->arrayFilter[$filterKey], explode(',', $filterValue));
                $orOuter->addMust($optionKeyTerm);
            }
            $filtered = new \Elastica\Query\Filtered();
            $filtered->setFilter($orOuter);
            $finalQuery = new \Elastica\Query($filtered);
        }else{
            $finalQuery = new \Elastica\Query();
        }
        if(!empty($query)){
            $simpleQueryString = new \Elastica\Query\SimpleQueryString($query, array("description^2", "name"));
            $finalQuery->setQuery($simpleQueryString);
        }
        if ($this->session->get('order') != null) {
            $finalQuery->setSort(array($this->session->get('order') => array('order' => $this->session->get('orderBy'), "mode"=>"min")));
        }
        return $this->fosElasticProducts->find($finalQuery);
    }
}