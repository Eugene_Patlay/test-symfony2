<?php
namespace Parfums\ProductsBundle\ElasticRepository;

use Parfums\ProductsBundle\Service\Helper;
use Elastica\Type;

class ElasticProductFilterRepository
{
    /**
     * @var array $arrayFilter асоциативный массив, где ключ=параметр в строке запроса, а значение=соответсвующее значение в базе
     */
    private $arrayFilter = array();

    /**
     * @var \Elastica\Type
     */
    private $fosElasticIndex;

    public function __construct(Type $fosElasticIndexExampleProducts)
    {
        $this->fosElasticIndex = $fosElasticIndexExampleProducts;
        $this->arrayFilter = Helper::getArrayFilter();
    }

    /**
     * Получение обьекта "Elastica\ResultSet" для фильтра, исходя из уже отфильтраванных значений
     * @param array $filter массив фильтра
     * @param string $from по какому параметру делаем фасет
     * @param string $query строка с параметром поиска
     * @return object обьект "Elastica\ResultSet"
     */
    public function getFacetValueResult($filter, $from, $query)
    {
        unset($filter[$from]);//убираем из массива текущий массив параметров, что бы он не фильтровал сам по себе
        if (!empty($filter)) {
            $orOuter = new \Elastica\Filter\Bool();
            foreach ($filter as $filterKey => $filterValue) {
                $optionKeyTerm = new \Elastica\Filter\Terms();
                $optionKeyTerm->setTerms($this->arrayFilter[$filterKey], explode(',', $filterValue));
                $orOuter->addMust($optionKeyTerm);
            }
            $filtered = new \Elastica\Query\Filtered();
            $filtered->setFilter($orOuter);
            $elasticQuery = new \Elastica\Query($filtered);
        } else {
            $elasticQuery = new \Elastica\Query();
        }
        $paramsFacet = new \Elastica\Facet\Terms($from);
        $paramsFacet->setField($this->arrayFilter[$from]);
        $paramsFacet->setSize(1000);
        $elasticQuery->addParam('_source', false);
        $elasticQuery->addFacet($paramsFacet);
        if (!empty($query)) {
            $simpleQueryString = new \Elastica\Query\SimpleQueryString($query, array("description^2", "name"));
            $elasticQuery->setQuery($simpleQueryString);
        }
        return $this->fosElasticIndex->search($elasticQuery);
    }
}