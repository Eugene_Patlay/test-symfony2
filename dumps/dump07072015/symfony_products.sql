CREATE DATABASE  IF NOT EXISTS `symfony` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `symfony`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: symfony
-- ------------------------------------------------------
-- Server version	5.6.24-0ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B3BA5A5A12469DE2` (`category_id`),
  KEY `IDX_B3BA5A5A44F5D008` (`brand_id`),
  CONSTRAINT `FK_B3BA5A5A12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_B3BA5A5A44F5D008` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Ноутбук Asus ZenBook UX301LA',35538,'Экран 13.3\" (1920x1080) FullHD Touch IPS, сенсорный, глянцевый / Intel Core i7-4500U (1.8 ГГц) / RAM 8 ГБ / 2x128 ГБ SSD (RAID0) / Intel HD Graphics 4400 / без ОД / Wi-Fi / Bluetooth / веб-камера / Windows 8 64bit / 1.4 кг / синий + сумка',1,1),(2,'Кофеварка эспрессо DELONGHI ESAM 3200 S',7499,'Тип: кофемашина эспрессо. Тип управления: электронное . Приготовление капучино: ручное. Тип используемого кофе: зерновой, молотый. Резервуар для воды: 1,8 л. Давление водяного насоса: 15 бар. Функции: автоматическая очистка от накипи, автоотключение, выбор объема порции, подогрев чашек, регулировка крепости кофе. Габариты: 28.5x37.5x36 см. Вес: 10 кг. Цвет: серебристый',2,3),(3,'Ноутбук Dell Latitude E5550',31346,'Экран 15.6\" (1920x1080) Full HD LED, матовый / Intel Core i5-4310U (2.0 ГГц) / RAM 8 ГБ / HDD 500 ГБ / Intel HD Graphics 4400 / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / Windows 8.1 Pro 64bit / 2.11 кг / черный',1,2),(4,'Ноутбук Asus X552EP',9460,'Экран 13.3\" (1366x768) HD LED, матовый / Intel Core i3-4010U (1.7 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics 4400 / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / DOS / 1.75 кг / серый',1,1),(5,'Кофеварка эспрессо SAECO HD8766/09',12629,'Тип: кофемашина эспрессо. Тип управления: электронный. Приготовление капучино: ручное. Тип используемого кофе: зерновой, молотый. Резервуар для воды: 1,9 л. Давление водяного насоса: 15 бар. Функции: автоматическая очистка от накипи, выбор объема порции, подача горячей воды для чая. Габариты: 25,6x47x35 см. Вес: 8,5 кг. Цвет: черный.',2,4),(6,'Микроволновая печь PANASONIC NN-SM221WZ',1450,'Тип: обычная (соло). Тип управления: механическое. Объем: 20 л. Мощность СВЧ: 800 Вт. Габариты (ВхШхГ): 25,8х44,3х34 см. Вес: 11 кг. Цвет: белый.',3,5),(7,'Микроволновая печь LG MS2022D',1699,'Тип: обычная (соло). Тип управления: механическое. Объем: 20 л. Мощность СВЧ: 700 Вт. Габариты (ВхШхГ): 26х45,5х33 см. Вес: 10 кг. Цвет: бел',3,6);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-07 17:24:04
